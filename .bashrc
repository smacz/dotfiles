alias dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles.git --work-tree=$HOME"
export VISUAL=vim

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
