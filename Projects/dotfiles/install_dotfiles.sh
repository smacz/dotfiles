#!/bin/bash

function dotfiles() {
        /usr/bin/git --git-dir=$HOME/.dotfiles.git --work-tree=$HOME $@
}

mkdir -p ~/Projects
git clone --recurse-submodules --separate-git-dir $HOME/.dotfiles.git https://www.gitlab.com/smacz/dotfiles.git;
rm -rf $HOME/dotfiles;
dotfiles reset --hard;
dotfiles config --local status.showUntrackedFiles no;
dotfiles submodule update;
