# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="andrewcz"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder
export ZSH_CUSTOM=${HOME}/.config/zsh

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git zsh-autosuggestions zsh-syntax-highlighting fzf-tab)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias vim="nvim"
#
#source /usr/share/rvm/scripts/rvm

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Puts timestamps in the shell history
setopt extendedhistory

alias dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles.git --work-tree=$HOME"
export VISUAL=nvim
export EDITOR=nvim

bindkey -v

export GPG_TTY=$(tty)

eval "$(ssh-agent)"
ssh-add ~/.ssh/id_ed25519

if [[ -f $HOME/.local/share/zsh/zshrc.local ]]; then
  source $HOME/.local/share/zsh/zshrc.local
fi

# Check if running on macOS
if [[ "$OSTYPE" == darwin* ]]; then
    # macOS specific commands or actions
    echo "Running on macOS"
    source /usr/local/Cellar/fzf/*/shell/key-bindings.zsh
    source /usr/local/Cellar/fzf/*/shell/completion.zsh
    ansible_from_top_level_dir() {
        # Save the current directory
        original_dir=$(pwd)
        # Change to the specified directory
        cd "$(git rev-parse --show-toplevel)"
        # Run ansible-playbook with the provided arguments
        OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES ANSIBLE_STDOUT_CALLBACK=debug ansible-playbook -e @~/.config/updox/devops.yml --diff ${@}
        # Return to the original directory
        cd "${original_dir}"
    }
    alias apb='ansible_from_top_level_dir'
    alias ans="OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES ANSIBLE_STDOUT_CALLBACK=debug ansible -e @~/.config/updox/devops.yml --diff"
    export PATH="/usr/local/opt/python@3.11/libexec/bin:$PATH"
fi

# Check if running on Manjaro
if [[ -f /etc/manjaro-release ]]; then
    # Manjaro specific commands or actions
    echo "Running on Manjaro"
    source /usr/share/fzf/key-bindings.zsh
    source /usr/share/fzf/completion.zsh
fi

# Check if running on Fedora
if [[ -f /etc/fedora-release ]]; then
    # Fedora specific commands or actions
    echo "Running on Fedora"
fi

# Check if running on Ubuntu
if [[ -f /etc/lsb-release && $(grep "DISTRIB_ID=Ubuntu" /etc/lsb-release) ]]; then
    # Ubuntu specific commands or actions
    echo "Running on Ubuntu"
fi

# Check if running on Debian
if [[ -f /etc/debian_version ]]; then
    # Debian specific commands or actions
    echo "Running on Debian"
fi

# Enable fzf-tab here since it needs to be the last completion sourced
enable-fzf-tab
# disable sort when completing `git checkout`
zstyle ':completion:*:git-checkout:*' sort false
# set descriptions format to enable group support
zstyle ':completion:*:descriptions' format '[%d]'
# set list-colors to enable filename colorizing
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
# preview directory's content with exa when completing cd
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'exa -1 --color=always $realpath'
# switch group using `,` and `.`
zstyle ':fzf-tab:*' switch-group ',' '.'
