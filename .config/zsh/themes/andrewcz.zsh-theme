# andrewcz prompt theme
autoload -U add-zsh-hook
autoload -Uz vcs_info

# Version control stuff
zstyle ':vcs_info:*' stagedstr '%F{green}●'
zstyle ':vcs_info:*' unstagedstr '%F{yellow}●'
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat '%b%F{1}:%F{11}%r'
zstyle ':vcs_info:*' enable git svn

precmd() {
    setopt noxtrace localoptions extendedglob prompt_subst
    local zero='%([BSUbfksu]|([FK]|){*})'

    #
    # Git
    #
    # Determine whether there are any new files in a git repo
    local vcs_format='%F{cyan}-%F{white}[%b%c%u%F{red}●%F{white}]'
    if [[ -z $(git ls-files --other --exclude-standard 2> /dev/null) ]] {
        # Somewhat backwards, but override it if there are no new files
        vcs_format='%F{cyan}-%F{white}[%b%c%u%F{white}]'
    }
    zstyle ':vcs_info:*' formats "${vcs_format}"
    # Refresh the git info
    vcs_info

    #
    # Prompt Length
    #
    vcs_width="${#${(S%%)vcs_info_msg_0_//$~zero/}}"
    if [[ "${vcs_width}" == 1 ]]; then
        # This will report a zero-length string as `1`, so we put the kabash on that
        vcs_width=0
    fi
    lprompt_1a_width=${#${(S%%)lprompt_1a//$~zero/}}
    lprompt_1c_width=${#${(S%%)lprompt_1c//$~zero/}}
    lprompt_1d_width=${#${(S%%)lprompt_1d//$~zero/}}
    padding_size=$(( COLUMNS - lprompt_1a_width - vcs_width - lprompt_1c_width - lprompt_1d_width ))
    prompt_padding="${(l:${padding_size}::-:)_empty_zz}"
    lprompt_1="$lprompt_1a$lprompt_1b$lprompt_1c$prompt_padding$lprompt_1d"
    # Try to fit in long path and user@host.
    if (( padding_size > 0 )); then
        :
    elif (( padding_size + lprompt_1d_width > 0 )); then
        # Didn't fit; try to fit in just long path and vcs info.
        padding_size=$(( padding_size + lprompt_1d_width ))
        prompt_padding="${(l:${padding_size}::-:)_empty_zz}"
        lprompt_1="$lprompt_1a$lprompt_1b$lprompt_1c$prompt_padding"
    elif (( padding_size + lprompt_1d_width + vcs_width > 0 )); then
        # Didn't fit; try to fit in just long path and vcs info.
        prompt_padding="${(l:${padding_size}::-:)_empty_zz}"
        lprompt_1="$lprompt_1a$lprompt_1c$prompt_padding"
    else
        # Still didn't fit; truncate 
        prompt_pwd_size=$(( COLUMNS - 5 ))
        lprompt_1="%F{cyan}.-<%$prompt_pwd_size<...<%~%<<]%F{cyan}-"
    fi

    #
    # Set Prompt String
    #
    PS1="$lprompt_1$lprompt_2%F{white}%(!.#.$)%F{cyan}> %f%k"
    RPS1="$rprompt"
}
prompt_opts=(cr subst percent)
lprompt_1a='%F{cyan}.-[%F{magenta}%~%F{white}'
lprompt_1b='${vcs_info_msg_0_}'
lprompt_1c='%F{cyan}>'
lprompt_1d="<%F{green}%n%B%F{black}@%b%F{red}%m%F{cyan}] "
lprompt_2='%F{cyan}\`--'
rprompt="%F{cyan}<%F{white}%D{%a, %b %d} %B%F{black}@%b %F{yellow}%D{%H:%M}%F{cyan}]"
add-zsh-hook precmd precmd
