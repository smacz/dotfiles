" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  "autocmd VimEnter * PlugInstall
  "autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/autoload/plugged')

" Manage the configs for each vim-plugged plugin in a separate file.
Plug 'ouuan/vim-plug-config'
" Better Syntax Support
" Plug 'sheerun/vim-polyglot'
" Even Better Syntax Support
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " We recommend updating the parsers on update
" Lightweight alternative to context.vim
Plug 'nvim-treesitter/nvim-treesitter-context'
" Auto pairs for '(' '[' '{'
Plug 'jiangmiao/auto-pairs'
" Git signs written in pure lua: https://github.com/lewis6991/gitsigns.nvim
Plug 'nvim-lua/plenary.nvim'
Plug 'lewis6991/gitsigns.nvim'
" A plugin to visualise and resolve conflicts in neovim
Plug 'akinsho/git-conflict.nvim'
" Neat-looking startup screen: https://github.com/mhinz/vim-startify
Plug 'mhinz/vim-startify'
" Rnvimr is a NeoVim plugin that allows you to use Ranger in a floating window.
Plug 'kevinhwang91/rnvimr', {'do': 'make sync'}
" Things you can do with fzf and Vim.
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim', { 'do': { -> fzf#install() } }
" Bright theme with pastel 'retro groove' colors
"Plug 'morhetz/gruvbox'
" Can try this one out later too
"Plug 'joshdick/onedark.vim'
" a dark color scheme for Vim heavily inspired by the look of the Dark+ scheme of Visual Studio Code.
"Plug 'tomasiser/vim-code-dark'
"Plug 'https://gitlab.com/color-schemes/vim-colors.git'
"Plug 'gryf/wombat256grf'
"Plug 'sainnhe/edge'
Plug 'sainnhe/everforest'
Plug 'numToStr/FTerm.nvim'
" This plugin adds indentation guides to all lines (including empty lines).
Plug 'lukas-reineke/indent-blankline.nvim'
" A (Neo)vim plugin for formatting code.
" Plug 'sbdchd/neoformat'
" A Vim plugin to visualizes the Vim undo tree.
Plug 'simnalamburt/vim-mundo'
" Set up projects by git repos
Plug 'airblade/vim-rooter'
" This is a vim syntax plugin for Ansible 2.x, it supports YAML playbooks, Jinja2 templates, and Ansible's hosts files.
" This is deprecated now that ansible is developing an LSP
" Plug 'pearofducks/ansible-vim', { 'do': './UltiSnips/generate.sh' }
" Far.vim makes it easier to find and replace text through multiple files. It's inspired by fancy IDEs, like IntelliJ and Eclipse, that provide cozy tools for such tasks.
" Plug 'ChristianChiarulli/far.vim'
" A search panel for neovim.
Plug 'windwp/nvim-spectre'
" An always-on highlight for a unique character in every word on a line to help you use f, F and family.
Plug 'unblevable/quick-scope'
" A vim port of emacs-which-key that displays available keybindings in popup.
Plug 'liuchengxu/vim-which-key', { 'on': ['WhichKey', 'WhichKey!'] }

" A blazing fast and easy to configure neovim statusline written in pure lua.
"Plug 'hoob3rt/lualine.nvim'
"Plug 'shadmansaleh/lualine.nvim'
Plug 'nvim-lualine/lualine.nvim'
" If you want to have icons in your statusline choose one of these
Plug 'kyazdani42/nvim-web-devicons'
" Plug 'ryanoasis/vim-devicons'
" This plugin shamelessly attempts to emulate the aesthetics of GUI text editors/Doom Emacs. It was inspired by a screenshot of DOOM Emacs using centaur tabs.
"Plug 'akinsho/bufferline.nvim'
" With bufexplorer, you can quickly and easily switch between buffers by using the one of the default public interfaces:
Plug 'jlanzarotta/bufexplorer'


" The plugin can show and remove trailing whitespaces.
Plug 'jdhao/whitespace.nvim'

" The best I can understand for auto-completion is that we need three things,
" and some of them repeat...
"
" 1. Autocompletion plugin
" 2. Source for engines to talk to/format for the autocomplete
" 3. Engines themselves (snippets sources, LSP)
"
" A completion engine plugin for neovim written in Lua. Completion sources are installed from external repositories and 'sourced'.
Plug 'hrsh7th/nvim-cmp'
" Snippet Engine for Neovim written in Lua. -- Snippets plugin
Plug 'L3MON4D3/LuaSnip'
" nvim-cmp source for neovim builtin LSP client -- LSP source for nvim-cmp
Plug 'hrsh7th/cmp-nvim-lsp'
" A collection of common configurations for Neovim's built-in language server client. This repo handles automatically launching and initializing language servers that are installed on your system.
Plug 'neovim/nvim-lspconfig'
" Companion plugin for nvim-lspconfig that allows you to seamlessly install LSP servers locally
Plug 'williamboman/nvim-lsp-installer'
"
" Snippets collection for a set of different programming languages for faster development.
Plug 'rafamadriz/friendly-snippets'
" luasnip completion source for nvim-cmp -- Snippets source for nvim-cmp
Plug 'saadparwaiz1/cmp_luasnip'
" nvim-cmp source for buffer words.
Plug 'hrsh7th/cmp-buffer'
" TODO: nvim-cmp source for filesystem paths.
"Plug 'hrsh7th/cmp-path'
" Syntax files for Solidity, the contract-oriented programming language for Ethereum.
Plug 'TovarishFin/vim-solidity'
Plug 'fatih/vim-go', { 'do:': ':GoUpdateBinaries' }
" Syntax highlighting for HashiCorp Configuration Language (HCL) used by Consul, Nomad, Packer, Terraform, and Vault.
Plug 'jvirtanen/vim-hcl'
call plug#end()
