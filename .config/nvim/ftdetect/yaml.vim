au BufRead,BufNewFile */playbooks/*.yml set filetype=yaml.ansible
" TODO: This doesn't work. Try to understand https://stackoverflow.com/a/5031937
au! BufRead,BufNewFile */nginx_kanboard.conf.j2 set filetype=nginx.jinja2
" TODO: Do more yaml.ansible stuff here, like roles/*/*/*.yml and
" roles/*/default/*.yml as regular yaml files
