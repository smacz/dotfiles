" set leader key
let g:mapleader = ","

syntax enable                           " Enables syntax highlighing
set hidden                              " Required to keep multiple buffers open
set nowrap                              " Display long lines as just one line
set encoding=utf-8                      " The encoding displayed
set pumheight=10                        " Makes popup menu smaller
set fileencoding=utf-8                  " The encoding written to file
set ruler                               " Show the cursor position all the time
set cmdheight=1                         " More space for displaying messages
set iskeyword+=-                        " treat dash separated words as a word text object"
set mouse=                              " Disable your mouse
set splitbelow                          " Horizontal splits will automatically be below
set splitright                          " Vertical splits will automatically be to the right
set t_Co=256                            " Support 256 colors
set conceallevel=0                      " So that I can see `` in markdown files
set tabstop=2                           " Insert 2 spaces for a tab
set shiftwidth=2                        " Change the number of space characters inserted for indentation
set smarttab                            " Makes tabbing smarter will realize you have 2 vs 4
set expandtab                           " Converts tabs to spaces
set smartindent                         " Makes indenting smart
set autoindent                          " Good auto indent
set laststatus=2                        " Always display the status line
set number                              " Line numbers
set relativenumber                      " Relative line numbers
set cursorline                          " Enable highlighting of the current line
set background=dark                     " tell vim what the background color looks like
set showtabline=0                       " Never show tabs - that's what we have a status line for, and bufexplorer
set noshowmode                          " We don't need to see things like -- INSERT -- anymore
set nobackup                            " This is recommended by coc
set nowritebackup                       " This is recommended by coc
set updatetime=300                      " Faster completion
set timeoutlen=500                      " By default timeoutlen is 1000 ms
set formatoptions-=cro                  " Stop newline continution of comments
"set clipboard=unnamedplus               " Copy paste between vim and everything else
"set autochdir                           " Your working directory will always be the same as your working directory
set scrolloff=3                         " Automatically scroll before reaching the bottom of the page
"set foldmethod=expr                     " Tree-sitter based folding
"set foldexpr=nvim_treesitter#foldexpr() " Tree-sitter based folding
set foldmethod=indent

au! BufWritePost $MYVIMRC source %      " auto source when writing to init.vm alternatively you can run :source $MYVIMRC

" You can't stop me
cmap w!! w !sudo tee %"

set title
"let &titlestring = printf('%svim:%%t%%r%%m', has('nvim') ? 'n' : '')
"set titlestring+=%(%{expand('%:p:~')}\ \ %)
set titlestring+=%(%{getcwd()}\ \ %)
set titlestring+=%{strftime('%Y-%m-%d\ %H:%M',getftime(expand('%')))}
"if $TERM =~ 'screen\|tmux'
"    let &titlestring = "\ek" . &titlestring . "\e\\"
"endif

" Working with italics and bolds
set t_ZH=[3m
set t_ZR=[23m

" Toggle off everything in the gutter so I can easily copy-paste stuff
function! ToggleNumber()
    if &number == 1
        set norelativenumber
        set nonumber
    else
        set number
        set relativenumber
    endif
endfunction

nnoremap <leader>n :call ToggleNumber()<CR>:Gitsigns toggle_signs<CR>:Gitsigns toggle_current_line_blame<CR>
