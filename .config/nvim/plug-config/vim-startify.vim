"testme
let g:startify_lists = [
      \ { 'type': 'sessions',  'header': ['   Sessions']       },
      \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
      \ { 'type': 'commands',  'header': ['   Commands']       },
      \ { 'type': 'files',     'header': ['   MRU']            },
      \ ]

let g:startify_commands = [
    \ {'u': ['Update Plugins', 'PlugUpdate']},
    \ ]

" Automatically update sessions
let g:startify_session_persistence = 1

" Similar to vim-rooter
let g:startify_change_to_vcs_root = 1
