" Autoformat on write. This is annoying for carefully-crafted config files,
" etc. Should be re-written to only take place on certain files.
"augroup fmt
"  autocmd!
"  autocmd BufWritePre * undojoin | Neoformat
"augroup END

let g:shfmt_opt="-ci"
