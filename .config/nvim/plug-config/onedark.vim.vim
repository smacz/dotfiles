if has('termguicolors')
      set termguicolors
endif

let g:onedark_termcolors = 256
let g:onedark_terminal_italics = 1

syntax on
" colorscheme onedark
