if has('termguicolors')
      set termguicolors
endif

set background=dark
let g:everforest_background = 'hard'
let g:everforest_enable_italic = 1
let g:everforest_ui_contrast = 'high'

colorscheme everforest

au BufRead,BufNewFile *.cash setfiletype solidity
