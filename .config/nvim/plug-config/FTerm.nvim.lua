local term = require("FTerm.terminal")

local gitui = term:new():setup({
    cmd = "gitui",
    dimensions = {
        height = 0.9,
        width = 0.9
    }
})

 -- Use this to toggle gitui in a floating terminal
function _G.__fterm_gitui()
    gitui:toggle()
end

-- Keybinding
local map = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }

map('', '<leader>t', '<CMD>lua _G.__fterm_gitui()<CR>', opts)
map('', '<leader>T', '<C-\\><C-n><CMD>lua _G.__fterm_gitui()<CR>', opts)
