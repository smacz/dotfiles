nnoremap <silent> <leader>r :RnvimrToggle<CR>
nnoremap <silent> <leader>R <C-\><C-n>:RnvimrResize<CR>
" Disable Rnvimr to import user configuration.
"let g:rnvimr_vanilla = 1
" Make Ranger replace Netrw and be the file explorer
let g:rnvimr_ex_enable = 1
" Make Neovim wipe the buffers corresponding to the files deleted by Ranger
let g:rnvimr_enable_bw = 1
" Try reloading the CWD every time we open
"let rnvimr_ranger_cmd = ['ranger', '--cmd=reload_cwd']
