vim.opt.termguicolors = true
require("bufferline").setup {
    options = {
        numbers = "buffer_id",
        diagnostics = "nvim_lsp",
        show_buffer_close_icons = false,
        separator_style = 'slant',
        sort_by = 'directory'
    }
}

vim.api.nvim_set_keymap('', '<leader>1', '<CMD>buffer 1<CR>', {noremap = true})
vim.api.nvim_set_keymap('', '<leader>2', '<CMD>buffer 2<CR>', {noremap = true})
vim.api.nvim_set_keymap('', '<leader>3', '<CMD>buffer 3<CR>', {noremap = true})
vim.api.nvim_set_keymap('', '<leader>4', '<CMD>buffer 4<CR>', {noremap = true})
vim.api.nvim_set_keymap('', '<leader>5', '<CMD>buffer 5<CR>', {noremap = true})
vim.api.nvim_set_keymap('', '<leader>6', '<CMD>buffer 6<CR>', {noremap = true})
vim.api.nvim_set_keymap('', '<leader>7', '<CMD>buffer 7<CR>', {noremap = true})
vim.api.nvim_set_keymap('', '<leader>8', '<CMD>buffer 8<CR>', {noremap = true})
vim.api.nvim_set_keymap('', '<leader>9', '<CMD>buffer 9<CR>', {noremap = true})
vim.api.nvim_set_keymap('', '<leader>0', '<CMD>buffer 10<CR>', {noremap = true})
