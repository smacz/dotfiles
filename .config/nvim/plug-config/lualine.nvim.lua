require('lualine').setup {
    options = {theme = 'everforest'},
    sections = {
        lualine_b = { { 'b:gitsigns_head', icon = '' } },
        lualine_c = { { 'filename', path = 1 } }
        -- lualine_c = { {'pwd'} },
    },
}
